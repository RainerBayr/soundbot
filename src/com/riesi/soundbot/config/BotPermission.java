package com.riesi.soundbot.config;

public enum BotPermission {

	owner(3),
	admin(2),
	moderator(1),
	user(0);
	
	private final int value;
	private BotPermission(int val) {
		value=val;
	}
	
	public int getPermissionValue() {
		return value;
	}
}
