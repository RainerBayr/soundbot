package com.riesi.soundbot.util;

/**
 * When something terrible happens and we need an Exception to handle it.
 * @author stefan
 *
 */
public class HaltCatchFire extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
